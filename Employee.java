
public class Employee implements Comparable<Employee>{ 

    String Emp_name;
    int Emp_id;
    
    public String getEmp_name() {
        return Emp_name;
    }
 
    public void setEmp_name(String Emp_name) {
        this.Emp_name = Emp_name;
    }
 
    public int getEmp_id() {
        return Emp_id;
    }
 
    public void setEmp_id(int Emp_id) {
        this.Emp_id = Emp_id;
    }
 
    Employee (String Emp_name, int Emp_id){
        this.Emp_name=Emp_name;
        this.Emp_id=Emp_id;
    }
 
   public int compareTo(Employee obj) {
        if (this.getEmp_id() == obj.getEmp_id())
        	{
        	return 0;
        	}
        else if (this.getEmp_id() < obj.getEmp_id())
        	{
        	return 1;
        	}
        else 
        	return -1;
            
   }
}
 

