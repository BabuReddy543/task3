
public class ThirdLargest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int temp, size;
		int arr[] = { 1, 8, 32, 6, 89, 90 };
		size = arr.length;
		for (int i = 0; i < size; i++) {
			for (int j = i + 1; j < size; j++) {
				if (arr[i] > arr[j]) {
					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
		System.out.println("The third largest element is:"+arr[size-2]);
	}

}
